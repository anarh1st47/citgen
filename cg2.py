#!/usr/bin/python2
# -*- coding: utf-8 -*-

# * Copyright 2016-2017 Dmitry Nikolaev
# * Licensed with GNU/GPLv3, see LICENSE file
# * This file is part of Citgen

# * Citgen is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.

# * Citgen is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# * GNU General Public License for more details.

# * You should have received a copy of the GNU General Public License
# * along with Citgen. If not, see <http://www.gnu.org/licenses/>.

from PIL import Image, ImageDraw, ImageFont, ImageFilter
import base64 #responsing it to API
import io #working with bytes
import urllib #downloading picture
from time import time #gallery
import cgi #web api
import textwrap #wrapping text(yes!!!11)
import re

def wrap(text, width=36):
  #text = "\""+text+"\""
  textLines = text.split('\n')
  respText = ""
  for line in textLines:
    respText+= textwrap.fill(line, width=width)+"\n"
  return respText

def shellquote(s):
    return s.replace("'", "'\\''").replace(";", "\;").replace("&", "\&").replace("|", "\|")
def toS(s):
    return shellquote(s).decode('utf-8')
def toArr(a):
    arr = []
    for i in range(len(a)):
        arr.append(toS(a[i]))
    return arr

print("Content-type: text/html\n")

form = cgi.FieldStorage()

try:
  data = [form["url"].value, form["text"].value, form["author"].value]
  url, text, auth  = toArr(data)
except:
  print("bad request")
text = wrap("\""+text+"\"")
cntLines=1


for i in range(len(text)):
 if(text[i]=="\n"):
    cntLines+=1

if(url=="http://vk.com/images/camera_200.png"):
  if(auth!="Andrey Tyan"):
    url="https://pp.vk.me/c837735/v837735942/15905/FoTYAJWQyZk.jpg"


tmpIm = urllib.urlopen(url).read() #reading bytes from internet

im = Image.new("RGB", (640, 400), "black")
try:
  ava = Image.open(io.BytesIO(tmpIm))
except:
  print("URL ERROR")
  exit(1)

ava.thumbnail([200, 200], Image.ANTIALIAS) #resizing ava to 200xSMTH

im.paste(ava, (12, 80)) #pasting image
draw = ImageDraw.Draw(im)

font = {"title": ImageFont.truetype("DejaVuSans.ttf", 43),
        "text" : ImageFont.truetype("DejaVuSans.ttf", 18)
}
draw.text((20, 20), "Цитаты великих людей:".decode('utf-8'),(255,255,255),font=font['title'])
draw.text((220, (200-20*(0.75*cntLines) if 200-20*(0.75*cntLines)>=80 else 80)),  text,(255,255,255),font=font['text'])


draw.text((640-(11*(4+len(auth))), 354), u"(\u2184)"+auth, (255,255,255), font=font['text'])


imgByteArr = io.BytesIO()
im.save(imgByteArr, format='PNG')

print("<img src='data:image/png;base64," + base64.b64encode(imgByteArr.getvalue()) + "'></img>")

im.save("../tmp/mcgi_" + str(time()) + ".jpg")
