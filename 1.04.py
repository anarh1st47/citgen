#!/usr/bin/python3

# * Copyright 2016-2017 Dmitry Nikolaev
# * Licensed with GNU/GPLv3, see LICENSE file
# * This file is part of Citgen

# * Citgen is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.

# * Citgen is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# * GNU General Public License for more details.

# * You should have received a copy of the GNU General Public License
# * along with Citgen. If not, see <http://www.gnu.org/licenses/>.

from PIL import Image, ImageDraw, ImageFont, ImageFilter
import base64 #responsing it to API
import io #working with bytes
import urllib.request #downloading picture
from time import time #gallery
import cgi #web api
import textwrap #wrapping text(yes!!!11)
from translate import Translator
import sys
import random

class Data:
  def __init__(self, url, text, auth, saving, isSelf):
    self.auth=auth
    self.text=text
    self.url =url
    self.saving=saving
    self.isSelf=isSelf
    

class Lib:
  def wrap(text, width=36):
    textLines = text.split('\n')
    respText = ""
    for line in textLines:
      respText+= textwrap.fill(line, width=width)+"\n"
    return respText
    
  def cntL(data):
    cntLines=1
    for i in range(len(data.text)):
      if(data.text[i]=="\n"):
        cntLines+=1
    return cntLines 
  
  def replaceNoobs(data):
    if(data.auth== "Maxim Filosov"):
      data.auth="Жопа"
    if(int(data.isSelf)):
      data.url = "https://anarh1st47.com/citgen/api/example.jpg"
    elif(data.url=="http://vk.com/images/camera_200.png"):
      if(data.auth!="Andrey Tyan"):
        data.url="https://pp.vk.me/c837735/v837735942/15905/FoTYAJWQyZk.jpg"
    return data

class CgiObj:
  def __init__(self):
    print("Content-type: text/html\n")
  def p(self, t):
    print(t)

class Consts:
  font = {"title": ImageFont.truetype("DejaVuSans.ttf", 43),
          "text" : ImageFont.truetype("DejaVuSans.ttf", 18)
  }
  white = (255,255,255)
  titlePos = (20, 20)
  textXPos = 220
  def textYPos(cntLines):
    return 200-20*(0.75*cntLines)
  def textPos(cntLines):
    return (Consts.textXPos, (Consts.textYPos(cntLines) if Consts.textYPos(cntLines)>=80 else 80))
  def authPos(addMore, data):
    return (640-(11*(4+addMore+len(data.auth))), 354)
  copyleft = "(\u2184)"
  colors = ["green", (0x49, 0x83, 0x6a), (0x4e, 0x9d, 0xc6), (0x8c, 0x83, 0x5f),
  (0xb1, 0x4d, 0x48), (0xa0, 0x53, 0x7f), (0x4d, 0x58, 0xac), (0xa7, 0x21, 0x40)]

def main():
  resp = CgiObj()
  form = cgi.FieldStorage()
  try:
    data = Data("https://anarh1st47.com/citgen/api/example.jpg", "elite test", "Dimas", "0", "0")
    #data = Data(form["url"].value, form["text"].value, form["auth"].value, 
    #            form["saving"].value, form["isSelf"].value)
  except:
    resp.p("bad request")

  #1st april
  translator= Translator(from_lang="ru", to_lang="zh")
  data.text = translator.translate(data.text)
  translator= Translator(from_lang="zh", to_lang="ru")
  data.text = translator.translate(data.text)

  data.text = Lib.wrap('"'+data.text+'"')
  cntLines = Lib.cntL(data)

  data = Lib.replaceNoobs(data)
  
    
  ####################################################
  tmpIm = urllib.request.urlopen(data.url).read() #reading bytes from internet


  im = Image.new("RGB", (640, 400), Consts.colors[random.randint(0, len(Consts.colors)-1)])
  try:
    ava = Image.open(io.BytesIO(tmpIm))
  except:
    resp.p("URL ERROR")
    exit(1)

  ava.thumbnail([200, 300], Image.ANTIALIAS) #resizing ava to 200x300

  im.paste(ava.rotate(random.randint(0, 359), expand=1), (12, 80)) #pasting image
  draw = ImageDraw.Draw(im)
  
  draw.text(Consts.titlePos, "Цитаты недалёких людей:", Consts.white, font=Consts.font['title'])
  draw.text(Consts.textPos(cntLines), data.text, Consts.white, font=Consts.font['text'])

  if(not int(data.isSelf)):
    addMore = 0
  else:
    addMore = 10
    Consts.copyleft = "self signed " + Consts.copyleft
  draw.text(Consts.authPos(addMore, data), Consts.copyleft+data.auth, Consts.white, font=Consts.font['text'])

  imgByteArr = io.BytesIO()
  im.save(imgByteArr, format='JPEG')

  resp.p(base64.b64encode(imgByteArr.getvalue()).decode('UTF-8'))

  if(int(data.saving)):
	  dirToSave = "lel"
  else:
    dirToSave = "tmp"
  im.save("../" + dirToSave + "/mcgi_" + str(time()) + ".jpg")


main()

