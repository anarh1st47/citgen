/*
 * Copyright 2017 Dmitry Nikolaev
 * Licensed with GNU/GPLv3, see LICENSE file
 * This file is part of Citgen
 * Citgen is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Citgen is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Citgen. If not, see <http://www.gnu.org/licenses/>.
 */
package imba.citgen

import scala.io.Source
import scala.math // max
import java.net.URL
import java.io.File
import javax.imageio.ImageIO
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.image.BufferedImage

/*import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.canvas
import com.sksamuel.scrimage.ParImage*/
import org.apache.commons.lang3.text.WordUtils //wrapper
import org.imgscalr.Scalr //resizing



class Data(url:URL, text:String, auth:String, saving:Int, isSelf:Int){
	def getUrl  : URL = this.url
	def getText : String = WordUtils.wrap('"' + this.text + '"', 36, "\n", true).mkString
	def getAuth : String = this.auth
	def getSave : Int = this.saving
	def getSelf : Int = this.isSelf
	def getCntLines : Int = this.getText.split("\n").length
}

object Consts{
	val titlePos = (20, 20)
	val textXPos = 220
	def textYPos(cntLines: Int) = Math.max(200-20*(0.75*cntLines), 80)
	def authPos(addMore: Int, data: Data) = 640-(11*(4+addMore+data.getAuth.length))
	val copyleft = "(\u2184)"
	val badUrl = new URL("http://svalko.org/data/2014_01_11_11_34_334190_1.jpeg")
}


object Main extends App {
	def drawString(g: Graphics2D, text: String, x: Int, y: Int) {
		var yy: Int = y
		for (line <- text.split("\n")){
			yy += g.getFontMetrics.getHeight
			g.drawString(line, x, yy)
		}
	}

	def get(url: String) = scala.io.Source.fromURL(url).mkString
	val data = new Data(new URL("https://pp.userapi.com/c633729/v633729873/29f95/xXmXW36OpG0.jpg"), "example", "Dimas", 0, 0)
	/*Хуета, которая работала через уебанскую либу, которая не отдавала нихуя кроме своего объекта
	val f = new File("scrimage.jpg")
	Image.fromStream(data.getUrl.openStream).scaleToWidth(200).output(f)
	val ava  = ImageIO.read(f)*/
	val ava = try ImageIO.read(data.getUrl) catch {
		case e: Throwable => ImageIO.read(Consts.badUrl)
	}
	val scaledImage: BufferedImage = Scalr.resize(ava, 200)
	/*Хуета, которая пиздец как шакалила
	val ava = ImageIO.read(data.getUrl)
	val coef = ava.getWidth/200.toDouble
	val outputImage = new BufferedImage(200, (ava.getHeight.toDouble/coef).toInt, ava.getType)
	val g2d:Graphics2D = outputImage.createGraphics()
	g2d.drawImage(ava, 0, 0, 200, (ava.getHeight.toDouble/coef).toInt, null)
	g2d.dispose()
	ImageIO.write(outputImage, "png", new File("g2d.png"))*/
	
	val bufferedImage = new BufferedImage(640, 400, BufferedImage.TYPE_INT_RGB)
	val graphics: Graphics2D = (bufferedImage.createGraphics())
	graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
	graphics.setColor(Color.WHITE)
	graphics.setFont(new Font("DejaVuSans.ttf", Font.PLAIN, 43))
	graphics.drawString("Цитаты великих людей:", 20, 61)
	graphics.drawImage(scaledImage, 12, 80, 200, scaledImage.getHeight, null)
	graphics.setFont(new Font("DejaVuSans.ttf"/*"Arial Black"*/, Font.PLAIN, 18))
	drawString(graphics, data.getText, Consts.textXPos, Consts.textYPos(data.getCntLines).toInt+16)
	graphics.drawString(Consts.copyleft+data.getAuth, Consts.authPos(0, data).toInt, 354+16)
	ImageIO.write(bufferedImage, "png", new File("huj2.png")) 
}
