name := "citgen-scala"
version := "1.0"
scalaVersion := "2.11.8"


resolvers += "adobe" at "https://repo.adobe.com/nexus/content/repositories/public/"
resolvers += "huj" at "http://central.maven.org/maven2/"

libraryDependencies ++= Seq(
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.8",
  "org.apache.commons" % "commons-lang3" % "3.5",
  "org.imgscalr" %"imgscalr-lib" % "4.2"
)
